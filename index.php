<?php
  require_once('animal.php');
  require_once('ape.php');
  require_once('frog.php');

  $sheep = new Animal("Shaun");

  echo "Name :".$sheep->name."<br>";
  echo "legs : ".$sheep->legs."<br>";
  echo "Cold Blooded : ".$sheep->cold_blooded."<br>";
  echo "<br>";

  $sungokong = new Ape ("Kera Sakti");
  echo "Name : ".$sungokong ->name."<br>";
  echo "legs : ".$sungokong ->legs."<br>";
  echo "Cold Blooded : ".$sungokong ->cold_blooded."<br>";
  echo $sungokong ->yell();
  echo "<br>";
  echo "<br>";

  $kodok = new Frog("buduk");
  echo "Name :".$kodok->name."<br>";
  echo "legs :".$kodok->legs."<br>";
  echo "Cold blooded :".$kodok->cold_blooded."<br>";
  echo $kodok->jump();
 ?>
